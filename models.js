require('./modules/config/config.model');
require('./modules/dogs/dog.model');
require('./modules/users/user.model');
require('./modules/chats/chat.model');
require('./modules/complaints/complaints.model');
require('./modules/handlers/error.handler.model');
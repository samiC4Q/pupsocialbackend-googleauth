const bodyParser = require('body-parser');
const passport = require("passport");
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const cookieParser = require('cookie-parser');
const config = require('./config/main');

const sessionParser = session({
    store: new MongoStore({url: config.database}),
    secret: config.session.secret,
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: config.session.cookie.maxAge
    },
    name: 'session_id'
});

module.exports = app => {
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(sessionParser);
    app.use(passport.initialize());
    app.use(passport.session());

    return {
        cookieParser,
        sessionParser
    }
};
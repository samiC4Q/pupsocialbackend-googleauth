const express = require('express');
const mongoose = require('mongoose');
const config = require('./config/main');
const exphbs = require('express-handlebars');



const app = express();

require('./models');
const middlewares = require('./middlewares')(app);
require('./routes')(app);
// require('./modules/develop')(app, mongoose);

/* DB */
mongoose.connect(config.database, error => {
    if (error) console.log('connection error:', error.message);
});

// Map global promises
mongoose.Promise = global.Promise;
// Mongoose Connect
mongoose.connect(config.database, {
  useMongoClient:true
})
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));
  
/* Error handlers*/
app.use(function (req, res, next) {
    res.status(404);
    res.send({status: false, data: 'Url not found'});
});

app.use(function (err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }

    res.status(err.status || 500);
    res.send({error: err.message});
});

app.use((req, res, next) => {
    res.locals.user = req.user || null;
    next();
  });
// Set static folder
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

/* Init */
const port = process.env.PORT || 8888;
const server = app.listen(port);

require('./modules/chats/chats.ws')(server, middlewares);
require('./cron-jobs');


const User = require('mongoose').model('User');
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');

const userUnselectedFields = '-isPushEnabled -defaultMeasureType -isProfileVisible -friends -facebookId -__v';

const getDistance = (lat1, lon1, lat2, lon2, unit = 1) => {
    let radlat1 = Math.PI * lat1 / 180;
    let radlat2 = Math.PI * lat2 / 180;
    let theta = lon1 - lon2;
    let radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    if (unit === 0) {
        dist = dist * 1.609344
    }

    return Math.round(dist);
};

module.exports = {
    async search(req, res) {
        try {
            const {user} = req;
            const [defaultLongitude, defaultLatitude] = user.location_coordinates || [];

            if (!user.isProfileVisible) {
                return res.json(responseHandler('data', []));
            }

            const {
                location_coordinates = '',
                search_distance = 50,
                relationships_status,
                owner_gender,
                age_from = 18,
                age_to = 90,
                isMapSearch = 0,

                dog_gender,
                dog_breeds,
                dog_sizes
            } = req.query;

            const [
                longitude = defaultLongitude,
                latitude = defaultLatitude
            ] = location_coordinates.split(',');

            let excludedUsers = [...user.blocked, user._id];

            if (+isMapSearch === 0) excludedUsers = [...excludedUsers, ...user.friends];

            const userQuery = {
                _id: {
                    $not: {
                        $in: excludedUsers
                    }
                },
                age: {
                    $gte: age_from,
                    $lte: age_to
                },
                isDeleted: false,
                isProfileVisible: true
            };

            const dogQuery = {
                path: 'dogs',
                match: {}
            };

            const ownersQuery = {
                path: 'owners',
                match: {},
                select: 'name age image_url relationships'
            };

            const otherOwnersQuery = {
                path: 'otherOwners',
                match: {},
                select: 'name image_url birthday relationships'
            };

            if (relationships_status) userQuery.relationships = {$in: relationships_status.split(',')};
            //Reverse gender, because of UI issue. In user model 1 is female and 2 is male.
            if (owner_gender && owner_gender != 0) userQuery.gender = (owner_gender == 1 ? 1 : 0); //0 - all, 1 - male, 2 - female
            if (dog_gender && dog_gender != 0) dogQuery.match.gender = (dog_gender == 1 ? 0 : 1); //0 - all, 1 - male, 2 - female
            if (dog_breeds) dogQuery.match.breed = {$in: dog_breeds.split(',')};
            if (dog_sizes) dogQuery.match.size = {$in: dog_sizes.split(',')};

            if (longitude && latitude) {
                const distanceToRadiansCoefficient = (req.user.defaultMeasureType ? 3959 : 6371);
                userQuery.live_location_coordinates = {
                    $nearSphere: [longitude, latitude],
                    $maxDistance: search_distance / distanceToRadiansCoefficient
                };
            }

            let users = await User.find(userQuery)
                .select(userUnselectedFields)
                .populate(dogQuery)
                .populate(ownersQuery)
                .populate(otherOwnersQuery)
                .exec();

            users = users.filter(searchUser => searchUser.dogs.length && searchUser.blocked.indexOf(user._id) === -1); //Filter users without dogs
            users = users.map(user => {
                const isFriend = req.user.friends.indexOf(user._id) !== -1;
                const userObject = user.toObject();

                delete userObject.blocked;
                let userCoordinates = user.live_location_coordinates;
                if (!userCoordinates) {
                    userCoordinates = user.location_coordinates;
                }
                return Object.assign({}, userObject, {
                    isFriend,
                    distance: getDistance(latitude, longitude, userCoordinates[1], userCoordinates[0], user.defaultMeasureType)
                });
            });

            res.json(responseHandler('data', users));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
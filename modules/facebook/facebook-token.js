const FacebookTokenStrategy = require('passport-facebook-token');
const User = require('mongoose').model('User');
const passport = require("passport");
const config = require('../../config/main');

passport.use(new FacebookTokenStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        profileFields: ["id", "birthday", "displayName", "email", "relationship_status", "picture"],
        scope: ["user_birthday", "email", "relationship_status", "picture"],

        profileImage: {
            height: 1000
        }
    }, function (accessToken, refreshToken, profile, done) {
        User.findOne({facebookId: profile._json.id}, function (err, user) {
            if (err) done(err);

            if (user) {
                done(null, user);
            } else {
                try {
                    const newUser = new User({
                        facebookId: profile._json.id,
                        email: profile._json.email,
                        name: profile._json.name,
                        birthday: profile._json.birthday,
                        image_url: `https://graph.facebook.com/v2.6/${profile._json.id}/picture?height=1000`
                    });

                    newUser.save(function (err, user) {
                        if (err) done(err);

                        done(null, user);
                    });
                } catch (err) {
                    done(err);
                }
            }
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, (err, user) => {
        err ? done(err) : done(null, user);
    });
});

module.exports = {
    authenticate: passport.authenticate('facebook-token')
};
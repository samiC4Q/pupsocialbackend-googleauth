const https = require('https');

module.exports = {
    getUserFriends(userId, access_token) {
        return new Promise((resolve, reject) => {
            const options = {
                host: 'graph.facebook.com',
                port: 443,
                path: `/v2.9/${userId}/friends?access_token=${access_token}`,
                method: 'GET'
            };

            const getReq = https.request(options, res => {
                res.on('data', data => {
                    try {
                        data = JSON.parse(data);

                        if (data.error) reject(data.error);

                        resolve(data.data);
                    } catch (err) {
                        reject(err);
                    }
                });
            });

            getReq.end();
            getReq.on('error', reject);
        });
    }
};
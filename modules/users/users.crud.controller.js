const mongoose = require('mongoose');
const User = mongoose.model('User');
const Dog = mongoose.model('Dog');
const Chat = mongoose.model('Chat');
const uniqid = require('uniqid');

const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');
const amazonS3 = require('../amazon/amazon.controller');
const ObjectId = mongoose.Types.ObjectId;

const protectedFields = ['dogs', 'owners', 'friends', 'invited', 'blocked', 'facebookId', 'hangouts'];

module.exports = {
    async getUser(req, res) {
        try {
            let user = await req.user.populateFields();

            res.json(responseHandler('data', user));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async updateUser(req, res) {
        try {
            let {user} = req;

            protectedFields.forEach(fieldName => {
                delete req.body[fieldName];
            });

            user.set(req.body);

            await user.save();

            user = await user.populateFields();

            res.json(responseHandler('data', user));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getUserNotifications(req, res) {
        try {
            const {user} = req;

            let notifications = await user.populateNotifications();

            res.json(responseHandler('data', notifications));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getUserNotificationsStatus(req, res) {
        try {
            const {user} = req;

            const hasUnreadNotifications = user.notifications.find(notification => !notification.wasRead);

            res.json(responseHandler('data', {
                status: !!hasUnreadNotifications
            }));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async markUserNotificationsAsRead(req, res) {
        try {
            const {user} = req;

            user.notifications.forEach(notification => {
                notification.wasRead = true;
            });

            await user.save();

            res.json(responseHandler('user.notificationsUpdated'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async removeUser(req, res) {
        try {
            const {user} = req;

            await User.update(
                {},
                {
                    $pull: {
                        notifications: {
                            from: user._id
                        },
                        owners: user._id,
                        friends: user._id,
                        invited: user._id,
                        hangoutInvites: user._id,
                        blocked: user._id,
                        otherOwners: user._id
                    }
                },
                {multi: true}
            );

            await Chat.remove({
                members: {
                    $elemMatch: {
                        isCreator: true,
                        user: {
                            $eq: user._id
                        }
                    }
                }
            });

            await Chat.update(
                {},
                {
                    $pull: {
                        members: {
                            user: user._id
                        }
                    }
                },
                {multi: true}
            );

            await user.update({
                $unset: {pushToken: 1},
                facebookId: uniqid(),
                isDeleted: true,
                isProfileVisible: false
            });

            await amazonS3.remove(user.image_url);

            req.session.destroy();
            res.json(responseHandler('user.removed'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getUserDetail(req, res) {
        try {
            const {userId} = req.params;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let user = await User.findById(userId);

            if (!user) return res.json(errorHandler('user.notFound', {}, req.originalUrl));

            user = await user.populateFields();

            const isFriend = req.user.friends.indexOf(user._id) !== -1;

            const privateChatQuery = {
                members: {
                    $size: 2,
                    $all: [{$elemMatch: {user: mongoose.Types.ObjectId(userId)}}, {$elemMatch: {user: req.user._id}}]
                },
                isGroup: false
            };

            let chat = await Chat.findOne(privateChatQuery);

            let currentUser = await req.user.populateFields();

            res.json(responseHandler('data', Object.assign({}, user.toObject(), {
                isFriend,
                currentUser,
                chatId: chat ? chat._id : ''
            })));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getUserDetailNotifications(req, res) {
        try {
            const {userId} = req.params;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let user = await User.findById(userId);

            if (!user) return res.json(errorHandler('user.notFound', {}, req.originalUrl));

            let notifications = await user.populateNotifications();

            res.json(responseHandler('data', notifications));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async inviteContact(req, res) {
        try {
            let {user} = req;
            const {contact} = req.body;

            if (!contact) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            user.invitedContacts.push(contact);

            user = await req.user.save();
            user = await user.populateFields();

            res.json(responseHandler('data', user));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
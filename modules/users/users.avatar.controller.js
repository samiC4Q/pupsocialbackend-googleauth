const User = require('mongoose').model('User');
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');
const multer = require('multer');
const S3Storage = require('../amazon/amazon.storage');

module.exports = {
    uploader: multer({
        storage: S3Storage({
            getFilePath: function (req, file, cb) {
                cb(null, `avatars/${req.user._id}.${file.originalname.split('.').pop()}`);
            }
        })
    }).single('image'),

    async update(req, res) {
        try {
            let {user} = req;
            if (!req.file) return res.json(errorHandler('file.uploadError', {}, req.originalUrl));

            user.image_url = req.file.Location;
            await user.save();

            res.json(responseHandler('data', {
                image_url: user.image_url
            }));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
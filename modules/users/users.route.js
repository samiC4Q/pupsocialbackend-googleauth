const express = require('express');
const passport = require("passport");

const router = express.Router();

const usersCrudController = require('./users.crud.controller');
const usersFriendsController = require('./users.friends.controller');
const usersBlockedController = require('./users.blocked.controller');
const usersAvatarController = require('./users.avatar.controller');

router.get('/me', usersCrudController.getUser);
router.post('/me', usersCrudController.updateUser);
router.delete('/me', usersCrudController.removeUser);
router.get('/me/notifications', usersCrudController.getUserNotifications);
router.get('/me/notifications/get-status', usersCrudController.getUserNotificationsStatus);
router.post('/me/notifications/mark-as-read', usersCrudController.markUserNotificationsAsRead);

router.post('/invite-contact', usersCrudController.inviteContact);

router.post('/image', usersAvatarController.uploader, usersAvatarController.update);
router.get('/:userId', usersCrudController.getUserDetail);
router.get('/:userId/notifications', usersCrudController.getUserDetailNotifications);

router.post('/invite/:userId', usersFriendsController.inviteToFriends);
router.delete('/invite/:userId', usersFriendsController.unInviteToFriends);
router.delete('/invite/decline/:userId', usersFriendsController.declineFriendInvitation);

router.post('/friends/add-other-owners', usersFriendsController.addFriendsAsOtherOwners);
router.post('/friends/remove-other-owner', usersFriendsController.removeFriendFromOtherOwner);
router.get('/friends/facebook', usersFriendsController.getFacebookFriends);
router.post('/friends/contacts', usersFriendsController.getContactsFriends);
router.post('/friends/:userId', usersFriendsController.addToFriends);
router.delete('/friends/:userId', usersFriendsController.removeFromFriends);

router.post('/block_user/:userId', usersBlockedController.blockUser);
router.delete('/block_user/:userId', usersBlockedController.unlockUser);



module.exports = router;
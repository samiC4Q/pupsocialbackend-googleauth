const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: String,
    about: String,
    birthday: Date,
    location_address: String,
    phoneNumber: String,
    age: Number,
    image_url: String,
    pushToken: String,
    isPushEnabled: {
        type: Boolean,
        default: true
    },
    isProfileVisible: {
        type: Boolean,
        default: true
    },

    defaultMeasureType: {
        type: Number,
        enum: [0, 1], //0 - Kilometers, 1 - Miles
        default: 1
    },
    gender: {
        type: Number,
        enum: [0, 1] //0 - female, 1 - male
    },
    location_coordinates: {
        type: [Number],
        index: '2d'
    },
    live_location_coordinates: {
        type: [Number],
        index: '2d'
    },
    relationships: {
        type: Number,
        enum: [0, 1, 2, 3] //0 - none, 1 - single, 2 - in a relationship, 3 - married
    },

    dogs: [{type: Schema.Types.ObjectId, ref: 'Dog'}],
    owners: [{type: Schema.Types.ObjectId, ref: 'User'}],
    friends: [{type: Schema.Types.ObjectId, ref: 'User'}],
    invited: [{type: Schema.Types.ObjectId, ref: 'User'}],
    hangoutInvites: [{type: Schema.Types.ObjectId, ref: 'User'}],
    blocked: [{type: Schema.Types.ObjectId, ref: 'User'}],

    facebookId: {
        type: String,
        unique: true
    },  
    googleID: {
        type: String,
        unique:true
    },
    hangouts: {
        freeUsed: {
            type: Number,
            default: 0
        },
        freeAvailable: {
            type: Number,
            default: 25
        },
        purchasedUsed: {
            type: Number,
            default: 0
        },
        purchasedAvailable: {
            type: Number,
            default: 0
        }
    },
    otherOwners: {
        type: [{type: Schema.Types.ObjectId, ref: 'User'}],
        validate: [val => val.length <= 3, '{PATH} exceeds the limit of 3']
    }, //Max length === 3
    notifications: [{
        type: {
            type: Number,
            //1 - hangout request received, 2 - hangout request accepted by opponent, 8 - hangout request accepted by user
            //3 - friend request received, 4 - friend request accepted by opponent, 7 - friend request accepted by me
            //5 - added to group chat, 9 - added to private chat
            //6 - added as owner
            enum: [1, 2, 3, 4, 5, 6, 7, 8, 9]
        },
        from: {type: Schema.Types.ObjectId, ref: 'User'},
        payload: Schema.Types.Mixed,
        updatedAt: {
            type: String,
            default: Date.now()
        },
        wasRead: {
            type: Boolean,
            default: false
        }
    }],
    invitedContacts: [String],
    isDeleted: {
        type: Boolean,
        default: false
    }
});

UserSchema.pre('save', function (next) {
    if (this.birthday) {
        const ageDifMs = Date.now() - new Date(this.birthday).getTime();
        const ageDate = new Date(ageDifMs);

        this.age = Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    next();
});

UserSchema.methods.populateFields = function populateFields() {
    return this.populate('dogs')
        .populate('friends', 'name image_url dogs blocked')
        .populate('otherOwners', 'name image_url birthday relationships age')
        .populate('blocked', 'name image_url dogs')
        .execPopulate()
        .then(user => {
            user.friends = user.friends.filter(friend => {
                const isFriendBlocked = user.blocked.find(blocked => blocked._id.toString() === friend._id.toString());
                const isUserBlocked = friend.blocked.find(blockedId => blockedId.toString() === user._id.toString());

                return !isFriendBlocked && !isUserBlocked;
            });

            return this.model('Dog')
                .populate(user, [
                    {
                        path: 'friends.dogs',
                        select: 'name images',
                        options: {
                            limit: 1
                        }
                    },
                    {
                        path: 'blocked.dogs',
                        select: 'name images',
                        options: {
                            limit: 1
                        }
                    }
                ]);
        });
};

UserSchema.methods.populateNotifications = function populateFields() {
    return this.populate('notifications.from', 'name image_url')
        .execPopulate()
        .then(user => {
            return user.notifications.map(notification => {
                return Object.assign({}, notification.toObject(), {
                    currentUserId: user._id
                })
            });
        });
};

mongoose.model('User', UserSchema);
module.exports = UserSchema;
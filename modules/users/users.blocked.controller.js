const mongoose = require('mongoose');
const User = mongoose.model('User');
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');
const Chat = mongoose.model('Chat');
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
    async blockUser(req, res) {
        try {
            const {userId} = req.params;
            let {user} = req;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.blocked.indexOf(userId) !== -1) return res.json(errorHandler('user.alreadyBlocked', {}, req.originalUrl));

            let blockedUser = await User.findById(userId);

            if (!blockedUser) return res.json(errorHandler('user.notFound', {}, req.originalUrl));

            user.blocked.push(blockedUser);
            user.otherOwners.pull(blockedUser);

            await user.save();

            const privateChatQuery = {
                members: {
                    $size: 2,
                    $all: [{$elemMatch: {user: mongoose.Types.ObjectId(blockedUser._id)}}, {$elemMatch: {user: user._id}}]
                },
                isGroup: false
            };

            let chat = await Chat.findOne(privateChatQuery);

            if (chat) {
                chat.disabled = true;
                await chat.save();
            }

            res.json(responseHandler('user.blocked'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async unlockUser(req, res) {
        try {
            const {userId} = req.params;
            let {user} = req;

            if (!userId) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.blocked.indexOf(userId) == -1) return res.json(errorHandler('user.notBlocked', {}, req.originalUrl));

            user.blocked.pull(userId);
            await user.save();

            const privateChatQuery = {
                members: {
                    $size: 2,
                    $all: [{$elemMatch: {user: mongoose.Types.ObjectId(userId)}}, {$elemMatch: {user: user._id}}]
                },
                isGroup: false
            };

            let chat = await Chat.findOne(privateChatQuery);

            if (chat) {
                chat.disabled = false;
                await chat.save();
            }

            res.json(responseHandler('user.unlocked'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
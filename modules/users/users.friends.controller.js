const mongoose = require('mongoose');
const User = mongoose.model('User');
const Chat = mongoose.model('Chat');
const Dog = mongoose.model('Dog');
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');
const facebookController = require('../facebook/facebook.controller');
const pushNotifications = require('../notifications/push-notifications.controller');
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
    async inviteToFriends(req, res) {
        try {
            const {userId} = req.params;
            const {user} = req;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.friends.indexOf(userId) !== -1) return res.json(errorHandler('user.alreadyInFriends', {}, req.originalUrl));

            let friend = await User.findById(userId);

            if (!friend) return res.json(errorHandler('user.notFound', {}, req.originalUrl));
            if (friend.invited.indexOf(user._id) !== -1) return res.json(errorHandler('user.alreadyInvited', {}, req.originalUrl));

            friend.invited.push(user);
            friend.notifications.unshift({
                type: 3,
                from: user
            });

            await friend.save();

            friend = await friend.populateFields();

            pushNotifications.send(friend.pushToken, {
                title: user.name,
                body: 'Sent a friend request',
                payload: {
                    imageUrl: user.image_url,
                    route: 'popup',
                    type: 3,
                    userId: user._id
                }
            });

            res.json(responseHandler('data', Object.assign({}, friend.toObject(), {
                isFriend: false
            })));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async declineFriendInvitation(req, res) {
        try {
            const {userId} = req.params;
            let {user} = req;

            if (!userId) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            const notification = user.notifications.find(notification => notification.type === 3 && notification.from.toString() === userId);

            user.invited.pull(userId);
            user.notifications.pull(notification);

            await user.save();

            user = await user.populateFields();

            res.json(responseHandler('data', user));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async unInviteToFriends(req, res) {
        try {
            const {userId} = req.params;
            const {user} = req;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.friends.indexOf(userId) !== -1) return res.json(errorHandler('user.alreadyInFriends', {}, req.originalUrl));

            let friend = await User.findById(userId);

            if (!friend) return res.json(errorHandler('user.notFound', {}, req.originalUrl));
            if (friend.invited.indexOf(user._id) === -1) return res.json(errorHandler('user.wasNotInvited', {}, req.originalUrl));

            friend.invited.pull(user);

            const notification = friend.notifications.find(notification => notification.type === 3 && notification.from.toString() === user._id.toString());

            friend.notifications.pull(notification);

            await friend.save();

            friend = await friend.populateFields();

            res.json(responseHandler('data', Object.assign({}, friend.toObject(), {
                isFriend: false
            })));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async addToFriends(req, res) {
        try {
            const {userId} = req.params;
            let {user} = req;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.friends.indexOf(userId) !== -1) return res.json(errorHandler('user.alreadyInFriends', {}, req.originalUrl));

            let friend = await User.findById(userId);

            if (!friend) return res.json(errorHandler('general.notFound', {}, req.originalUrl));
            if (user.invited.indexOf(userId) === -1) return res.json(errorHandler('user.wasNotInvited', {}, req.originalUrl));

            friend.notifications.unshift({
                type: 4,
                from: user._id.toString()
            });
            friend.friends.push(user._id.toString());

            await friend.save();

            let notification = user.notifications.find(notification => notification.type === 3 && notification.from.toString() === friend._id.toString());

            notification.type = 7;
            notification.updatedAt = Date.now();

            user.invited.pull(friend._id);
            user.friends.push(friend._id);

            user = await user.save();

            let notifications = await user.populateNotifications();

            pushNotifications.send(friend.pushToken, {
                title: user.name,
                body: 'Accepted your friend request',
                payload: {
                    imageUrl: user.image_url,
                    route: 'banner',
                    type: 4,
                    userId: user._id
                }
            });

            res.json(responseHandler('data', notifications));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async removeFromFriends(req, res) {
        try {
            const {userId} = req.params;
            let {user} = req;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.friends.indexOf(userId) === -1) return res.json(errorHandler('user.notInFriends', {}, req.originalUrl));

            let friend = await User.findById(userId);

            if (!friend) return res.json(errorHandler('general.notFound', {}, req.originalUrl));

            friend.friends.pull(user);
            user.friends.pull(friend);
            user.otherOwners.pull(friend);

            await friend.save();

            user = await user.save();
            //Remove private chat with this user
            const privateChatQuery = {
                members: {
                    $size: 2,
                    $all: [{$elemMatch: {user: mongoose.Types.ObjectId(userId)}}, {$elemMatch: {user: user._id}}]
                },
                isGroup: false
            };

            let chat = await Chat.findOne(privateChatQuery);

            friend = await friend.populateFields();

            const extendedFriend = Object.assign({}, friend.toObject(), {
                isFriend: false
            });

            if (chat) {
                await chat.remove();
            }

            res.json(responseHandler('data', extendedFriend));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getFacebookFriends(req, res) {
        try {
            const {access_token} = req.headers;
            const {user} = req;

            if (!access_token) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let fbUsers = await facebookController.getUserFriends(req.user.facebookId, access_token);

            const fbIds = fbUsers.map(fbUser => fbUser.id);

            fbUsers = await User.find({
                facebookId: {
                    $in: fbIds
                },
                _id: {
                    $not: {
                        $in: user.friends
                    }
                }
            });

            fbUsers = fbUsers.map(fbUser => {
                const {_id, image_url, name} = fbUser;

                const isInvited = (user.invited.indexOf(fbUser._id) !== -1 || fbUser.invited.indexOf(user._id) !== -1) ? 1 : 0;

                return {_id, image_url, name, isInvited};
            });

            res.json(responseHandler('data', fbUsers));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getContactsFriends(req, res) {
        try {
            const {phones} = req.body;

            if (!phones) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            const phonesRegExp = new RegExp(phones.replace(/[^\w\s]/gi, '.'));

            let users = await User.find({phoneNumber: phonesRegExp});

            users = users.map(user => {
                const {_id, image_url, name} = user;

                return {_id, image_url, name}
            });

            res.json(responseHandler('data', users));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async addFriendsAsOtherOwners(req, res) {
        try {
            let {user} = req;
            let {friendsIds} = req.body;

            if (!friendsIds || !friendsIds.length) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            friendsIds = friendsIds
                .filter(userId => user.friends.indexOf(userId) !== -1) //Not in friends
                .filter(userId => user.otherOwners.indexOf(userId) === -1); //Already in otherOwners

            user = await user.populateFields();

            const [firstDog] = user.dogs;

            let friends = await User.find({
                _id: {
                    $in: friendsIds
                }
            });

            const friendsPromises = await friends.map(async friend => {
                user.otherOwners.push(friend._id);

                friend.notifications.unshift({
                    type: 6,
                    from: user._id,
                    payload: {
                        gender: friend.gender,
                        dogName: firstDog.name
                    }
                });

                pushNotifications.send(friend.pushToken, {
                    title: user.name,
                    body: `Added you as ${friend.gender === 1 ? 'Papa' : 'Mama'} of ${firstDog.name}`,
                    payload: {
                        imageUrl: user.image_url,
                        route: 'banner',
                        type: 6,
                        userId: user._id,
                        gender: friend.gender,
                        dogName: firstDog.name
                    }
                });

                return await friend.save();
            });

            await user.save();

            user = await user.populateFields();

            res.json(responseHandler('data', user));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async removeFriendFromOtherOwner(req, res) {
        try {
            let {user} = req;
            const {friendId} = req.body;

            if (!friendId) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.friends.indexOf(friendId) === -1) return res.json(errorHandler('user.notInFriends', {}, req.originalUrl));
            if (user.otherOwners.indexOf(friendId) === -1) return res.json(errorHandler('user.notInOtherOwners', {}, req.originalUrl));

            user.otherOwners.pull(friendId);

            await user.save();

            user = await user.populateFields();

            res.json(responseHandler('data', user));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
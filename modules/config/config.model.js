const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConfigSchema = new Schema({
    name: String,
    maxVersion: String,
    minVersion: String,
    'terms&conditionsURL': String,
    privacyURL: String,
    supportEmail: String,
    privacyUrl: String,
    termsUrl: String,
    dog_breeds: Array,
    dog_sizes: Array
});

mongoose.model('Config', ConfigSchema);
module.exports = ConfigSchema;
const express = require('express');
const router = express.Router();
const configController = require('./config.controller');

router.get('/', configController.getConfig);

module.exports = router;
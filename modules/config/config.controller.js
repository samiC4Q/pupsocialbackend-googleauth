const mongoose = require('mongoose');
const responseHandler = require('../handlers/response.handler');
const Config = mongoose.model('Config');
const initConfig = require('../../config/app-init.config');

module.exports = {
    async getConfig(req, res) {
        try {
            const {name = 'app-config'} = req.params;

            let config = await Config.findOne({name});

            if (!config) {
                await new Config(initConfig).save();
            }

            res.json(responseHandler('data', config));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
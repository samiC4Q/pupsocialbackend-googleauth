const faker = require('faker');
const appConfig = require('../../config/app.json');

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function fetRandomLocationCoordinatesForRegion(lng = -73.9682, lat = 40.7850) {
    const r = (1000 * 100) /111300 // = 100 km
        , y0 = lat
        , x0 = lng
        , u = Math.random()
        , v = Math.random()
        , w = r * Math.sqrt(u)
        , t = 2 * Math.PI * v
        , x = w * Math.cos(t)
        , y1 = w * Math.sin(t)
        , x1 = x / Math.cos(y0);

    return [
        x0 + x1,
        y0 + y1
    ];
}

module.exports = {
    generateUserModel() {
        return {
            name: faker.name.findName(),
            about: faker.lorem.sentences(),
            birthday: faker.date.between('01-01-1958', '01-01-1998'),
            location_address: faker.address.streetAddress(),
            phoneNumber: faker.phone.phoneNumber(),
            image_url: faker.image.avatar(),
            defaultMeasureType: getRandomInt(0, 1),
            isPushEnabled: faker.random.boolean(),
            isProfileVisible: faker.random.boolean(),
            gender: getRandomInt(0, 1),
            location_coordinates: fetRandomLocationCoordinatesForRegion(),
            relationships: getRandomInt(0, 3),
            facebookId: faker.random.number()
        };
    },

    generateDogModel(index) {
        return {
            name: faker.name.findName(),
            about: faker.lorem.sentences(),
            birthday: faker.date.between('01-01-1958', '01-01-1998'),
            breed: appConfig.dog_breeds.slice(getRandomInt(-appConfig.dog_breeds.length, appConfig.dog_breeds.length)),
            images: [`http://loremflickr.com/1024/1024/dog?random=${index}`],
            gender: getRandomInt(0, 1),
            size: appConfig.dog_sizes[getRandomInt(0, appConfig.dog_sizes.length - 1)]
        };
    },

    getRandomInt
};
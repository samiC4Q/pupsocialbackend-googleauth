const generators = require('./generators');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Dog = mongoose.model('Dog');
const errorHandler = require('../handlers/error.handler');

module.exports = {
    generateFakeUsers(req, res) {
        const newUsers = new Array(100)
            .fill(null)
            .map(() => new User(generators.generateUserModel()).save());

        Promise.all(newUsers).then(
            data => res.json({status: true, data: `${data.length} users created`}),
            error => res.json(error)
        );
    },

    generateFakeDogs(req, res) {
        User.find({
            dogs: {$exists: true, $size: 0}
        }, (err, users) => {
            if (err) return errorHandler('db', err);
            let iteration = 0;

            Promise.all(
                users.map(user => {
                    new Array(generators.getRandomInt(1, 3)).fill(null).map(() => {
                        iteration++;

                        return new Dog(generators.generateDogModel(iteration)).save((err, dog) => {
                            user.dogs.push(dog);

                            return user.save();
                        });
                    })
                })
            ).then(
                data => res.json({status: true, data: `Dogs created for ${data.length} users`}),
                err => res.json(err)
            );
        });
    },

    cleanModels(req, res) {
        Promise.all([
            User.remove({}),
            Dog.remove({})
        ]).then(
            data => res.json({status: true, data: 'Models was removed'}),
            err => res.json(err)
        );
    },

    deleteFakeUsers(req, res) {
        User.remove({
            image_url: new RegExp('https://s3.amazonaws.com/uifaces/faces/twitter')
        }, (err) => {
            if (err) return errorHandler('db', err);

            res.json({status: true, data: 'Generated users was removed!'});
        });
    }
};
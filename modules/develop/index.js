const docs = require("express-mongoose-docs");

module.exports = (app, mongoose) => {
    docs(app, mongoose);
};
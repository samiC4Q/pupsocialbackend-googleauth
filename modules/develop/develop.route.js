const express = require('express');
const router = express.Router();
const developController = require('./develop.controller');

router.post('/generate/users', developController.generateFakeUsers);
router.post('/generate/dogs', developController.generateFakeDogs);
router.delete('/clean-models', developController.cleanModels);
router.delete('/delete/users', developController.deleteFakeUsers);

module.exports = router;
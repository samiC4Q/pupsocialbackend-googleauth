const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ErrorSchema = new Schema({
    key: String,
    error: Schema.Types.Mixed,
    path: String,
    date: {
        type: Date,
        default: Date.now()
    }
});

mongoose.model('Error', ErrorSchema);
module.exports = ErrorSchema;
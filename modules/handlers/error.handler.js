const errors = require('./errors.json');
const mongoose = require('mongoose');
const ErrorModel = mongoose.model('Error');

module.exports = function (key = '', error = {}, path = '') {
    let message = errors;

    key.split('.').forEach(errorPiece => {
        message = message ? message[errorPiece] : 'Unknown error!';
    });

    if (!message) {
        switch (key) {
            case 'db':
            case 'custom':
            case 'amazon':
            case 'facebook':
            case 'google':
                message = error;
                break;

            default:
                message = "Some Error occurred";
                break;
        }
    }

    console.error(key, error);

    new ErrorModel({
        key,
        error: JSON.stringify(error),
        path
    }).save();

    return {
        status: false,
        data: message
    }
};
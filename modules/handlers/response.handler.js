const messages = require('./messages.json');

module.exports = function(messageKey, data) {
    let message = messages;

    messageKey.split('.').forEach(messagePiece => {
        message = message ? message[messagePiece] : 'Success!';
    });

    if (!message) {
        switch (messageKey) {
            case 'data':
                message = data;
                break;
            default:
                message = "Success!";
                break;
        }
    }

    return {
        status: true,
        data: message
    };
};
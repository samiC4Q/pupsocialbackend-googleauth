const apn = require('apn');
const config = require('../../config/main');
const apnProvider = new apn.Provider({
    token: config.apn,
    production: config.production
});

module.exports = {
    send(to, pushObj) {
        if (!to) return;

        const note = new apn.Notification(Object.assign({}, {
            sound: 'default',
            topic: 'com.pupsocialapp'
        }, pushObj));

        apnProvider.send(note, to).then( (result) => {
            console.info('Push result: ', JSON.stringify(result));
        });
    }
};
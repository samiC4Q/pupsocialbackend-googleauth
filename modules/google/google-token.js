const GoogleStrategy = require('passport-google-oauth20').Strategy;
const config = require('../../config/main');
const passport = require("passport");

// Load user model
const User = require('mongoose').model('User');

passport.serializeUser((user, done) => {
    done(null, user._id);
  });

  passport.deserializeUser((id, done) => {
    User.findById(id).then(user => done(null, user));
  });

  passport.use(
    new GoogleStrategy({
      clientID: config.google.clientID,
      clientSecret:config.google.clientSecret,
      callbackURL:'/auth/google/callback',
      passReqToCallback: true,

    }, async (accessToken, refreshToken, profile, done) => {
      console.log(accessToken);
      console.log(profile);

      const image = profile.photos[0].value.substring(0, profile.photos[0].value.indexOf('?'));
      
      const newUser = {
        googleID: profile._json.id,
        name: profile._json.name,
        email: profile._json.email,
        image_url: image,
      }

      // Check for existing user
      User.findOne({
        googleID: profile._json.id
      }).then(user => {
        if(user){
          // Return user
          done(null, user);
        } else {
          // Create user
          new User(newUser)
            .save()
            .then(user => done(null, user));
        }
      })
    })
  );


  module.exports = {
    authenticate: passport.authorize('google-token')
};
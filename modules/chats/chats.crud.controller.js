const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');
const pushNotifications = require('../notifications/push-notifications.controller');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Chat = mongoose.model('Chat');
const Dog = mongoose.model('Dog');
const User = mongoose.model('User');

function populateChat(chat) {
    return new Promise((resolve, reject) => {
        chat.populate([
            {path: 'members.user', select: 'name image_url dogs blocked'},
            {path: 'members.invitedBy', select: 'name image_url dogs'},
            {path: 'messages.author', select: 'name image_url'},
        ], (err, chat) => {
            if (err) return reject(err);

            Dog.populate(chat, [
                {path: 'members.user.dogs', select: 'name images', options: {limit: 1}},
                {path: 'members.invitedBy.dogs', select: 'name images', options: {limit: 1}}
            ], (err, chat) => {
                if (err) return reject(err);

                User.populate(chat, [
                    {path: 'members.user.blocked', select: 'name image_url', options: {limit: 1}}
                ], (err, chat) => {
                    if (err) return reject(err);

                    resolve(chat)
                });
            });
        });
    });
}

module.exports = {
    async list(req, res) {
        try {
            const {user} = req;

            const query = {
                members: {
                    $elemMatch: {
                        user: {
                            $eq: user._id
                        }
                    }
                }
            };

            let chats = await Chat.find(query);

            chats = await Promise.all(chats.map(chat => populateChat(chat)));
            chats = chats.map(chat => chat.toObject());

            chats.forEach(chat => {
                const chatLastMessage = chat.messages[chat.messages.length - 1];
                const chatMember = chat.members.find(member => member.user._id.toString() === user._id.toString());
                const visibleMessages = chat.messages.filter(message => message.date >= chatMember.joinDate);

                chat.hasUnreadMessages = chatLastMessage && chatLastMessage.date > chatMember.exitDate;
                chat.isHidden = !chat.isGroup && !chatMember.isChatVisible;
                chat.messages = visibleMessages.slice(-500);
            });

            chats = chats
                .filter(chat => !chat.isHidden)
                .filter(chat => {
                    console.log('Chats.list id: ' + chat._id);
                    console.log('Chats.list chat.members: ' + JSON.stringify(chat.members));
                    const chatOpponent = chat.members.find(member => member.user._id.toString() !== user._id.toString());
                    if (!chatOpponent) {
                        return false;
                    }
                    const isUserBlocked = chatOpponent.user.blocked.find(opponent => opponent._id.toString() === user._id.toString());
                    return chat.isGroup ? true : user.blocked.indexOf(chatOpponent.user._id) === -1 && !isUserBlocked;
                });

            chats = chats
                .sort((prev, next) => {
                    const prevLastMessage = prev.messages[prev.messages.length - 1] || {};
                    const nextLastMessage = next.messages[next.messages.length - 1] || {};

                    return prevLastMessage.date < nextLastMessage.date;
                })
                .sort((prev, next) => {
                    return !prev.hasUnreadMessages && next.hasUnreadMessages ? 1 : 0;
                });

            res.json(responseHandler('data', chats));
        } catch (err) {
            console.error('Chats.list error: ' + JSON.stringify(err));
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async hasUnreadMessages(req, res) {
        try {
            const {user} = req;

            const query = {
                members: {
                    $elemMatch: {
                        user: {
                            $eq: user._id
                        }
                    }
                }
            };

            const chats = await Chat.find(query);

            const hasUnreadMessages = chats.find(chat => {
                const chatLastMessage = chat.messages[chat.messages.length - 1];
                const chatMember = chat.members.find(member => member.user.toString() === user._id.toString());
                return chat.members.length > 1 && chatLastMessage && chatLastMessage.date > chatMember.exitDate;
            });

            res.json(responseHandler('data', {
                status: !!hasUnreadMessages
            }));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async get(req, res) {
        try {
            const {chatId} = req.params;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));

            const chatMember = chat.members.find(member => member.user.toString() === user._id.toString());

            if (!chatMember) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));

            const chatLastMessage = chat.messages[chat.messages.length - 1];
            const visibleMessages = chat.messages.filter(message => message.date >= chatMember.joinDate);

            chat.hasUnreadMessages = chatLastMessage && chatLastMessage.date > chatMember.exitDate;
            chat.isHidden = !chat.isGroup && !chatMember.isChatVisible;
            chat.messages = visibleMessages.slice(-500);

            chat = await populateChat(chat);

            res.json(responseHandler('data', chat));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async create(req, res) {
        try {
            const {members, name, imageUrl} = req.body;
            const {user} = req;

            if (!members || !members.length) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            members.push(user._id);

            const data = {
                members: members.map(userId => {
                    const isCreator = userId.toString() === user._id.toString();

                    return {
                        user: userId,
                        invitedBy: user._id,
                        isCreator
                    };
                }),
                name,
                imageUrl
            };

            //Check if such private chat is already exist.
            if (members.length === 2) {
                const privateChatQuery = {
                    members: {
                        $size: 2,
                        $all: [{$elemMatch: {user: mongoose.Types.ObjectId(members[0])}}, {$elemMatch: {user: user._id}}]
                    },
                    isGroup: false
                };

                let chat = await Chat.findOne(privateChatQuery);

                if (chat) {
                    const chatMember = chat.members.find(member => member.user.toString() === user._id.toString());

                    if (!chatMember.isChatVisible) {
                        chatMember.isChatVisible = true;

                        chat = await chat.save();
                        chat = await populateChat(chat);

                        return res.json(responseHandler('data', chat));
                    } else {
                        return res.json(errorHandler('chats.alreadyExist', {}, req.originalUrl));
                    }
                } else {
                    let chat = await Chat.create(data);
                    let member = await User.findById(members[0]);

                    /*
                    member.notifications.unshift({
                        type: 9,
                        from: user._id,
                        payload: {
                            chatId: chat._id
                        }
                    });
                    */

                    member = await member.save();

                    /*
                    pushNotifications.send(member.pushToken, {
                        title: user.name,
                        body: `Added you to chat ${name ? name : ''}`,
                        payload: {
                            imageUrl: user.image_url,
                            route: 'banner',
                            type: 9,
                            userId: user._id,
                            chatId: chat._id
                        }
                    });
                    */

                    chat = await populateChat(chat);

                    res.json(responseHandler('data', chat));
                }
            } else {
                data.isGroup = true;

                let chat = await Chat.create(data);

                let members = await data.members
                    .filter(member => member.user.toString() !== user._id.toString())
                    .map(async member => {
                        member = await User.findById(member.user);

                        if (member) {
                            member.notifications.unshift({
                                type: 5,
                                from: user._id,
                                payload: {
                                    chatId: chat._id,
                                    chatName: name
                                }
                            });

                            member = await member.save();

                            pushNotifications.send(member.pushToken, {
                                title: user.name,
                                body: `Added you to group ${name ? name : ''}`,
                                payload: {
                                    imageUrl: user.image_url,
                                    route: 'banner',
                                    type: 5,
                                    userId: user._id,
                                    chatId: chat._id
                                }
                            });
                        }
                    });

                chat = await populateChat(chat);

                res.json(responseHandler('data', chat));
            }
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async update(req, res) {
        try {
            const {chatId} = req.params;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));

            const chatMember = chat.members.find(member => member.user.toString() === user._id.toString());

            if (!chatMember.isCreator) return res.json(errorHandler('chats.notOwner', {}, req.originalUrl));

            const {name, imageUrl} = req.body;

            chat.set({name, imageUrl});

            chat = await chat.save();
            chat = await populateChat(chat);

            res.json(responseHandler('data', chat));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async remove(req, res) {
        try {
            const {chatId} = req.params;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));

            const chatMember = chat.members.find(member => member.user.toString() === user._id.toString());

            if (!chatMember.isCreator) return res.json(errorHandler('chats.notOwner', {}, req.originalUrl));

            await chat.remove();

            res.json(responseHandler('chats.removed'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async leave(req, res) {
        try {
            const {chatId} = req.params;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));
            if (!chat.members.find(member => member.user.toString() === user._id.toString())) {
                return res.json(errorHandler('chats.notMember', {}, req.originalUrl));
            }

            const member = chat.members.find(member => member.user.toString() === user._id.toString());

            chat.members.pull(member);

            await chat.save();

            res.json(responseHandler('chats.leaved'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async invite(req, res) {
        try {
            const {chatId} = req.params;
            const {usersId} = req.body;
            const {user} = req;

            if (!usersId || !usersId.length) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));
            if (!chat.members.find(member => member.user.toString() === user._id.toString())) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));

            let users = await User.find({_id: {$in: usersId}});

            if (!users.length) return res.json(errorHandler('user.notFound', {}, req.originalUrl));

            await users.forEach(async invitedUser => {
                const existMember = chat.members.find(member => member.user.toString() === invitedUser._id.toString());

                if (!existMember) {
                    chat.members.push({
                        user: invitedUser._id,
                        invitedBy: user._id
                    });

                    invitedUser.notifications.unshift({
                        type: 5,
                        from: user._id,
                        payload: {
                            chatId: chat._id
                        }
                    });

                    pushNotifications.send(invitedUser.pushToken, {
                        title: user.name,
                        body: `Added you to group chat ${chat.name ? chat.name : ''}`,
                        payload: {
                            imageUrl: user.image_url,
                            route: 'banner',
                            type: 5,
                            userId: user._id,
                            chatId: chat._id
                        }
                    });

                    await invitedUser.save();
                }
            });

            await chat.save();

            chat = await populateChat(chat);
            res.json(responseHandler('data', chat));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async removeMember(req, res) {
        try {
            const {chatId} = req.params;
            const {memberId} = req.body;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (!memberId || !ObjectId.isValid(memberId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));

            const removedMember = chat.members.find(member => member._id.toString() === memberId);

            if (!removedMember) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));
            if (removedMember.invitedBy.toString() !== user._id.toString()) return res.json(errorHandler('chats.wasInvitedNotByUser', {}, req.originalUrl));

            chat.members.pull(removedMember);

            await chat.save();

            chat = await populateChat(chat);

            res.json(responseHandler('data', chat));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async updateNotificationSettings(req, res) {
        try {
            const {chatId} = req.params;
            const {isPushEnabled} = req.body;
            const {user} = req;

            if (!isPushEnabled) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));

            const member = chat.members.find(member => user._id.equals(member.user.toString()));

            if (!member) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));

            member.isPushEnabled = isPushEnabled;

            await chat.save();

            chat = await populateChat(chat);

            res.json(responseHandler('data', chat));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getChatMessages(req, res) {
        try {
            const {chatId} = req.params;
            const {messageId = 0, limit = 500} = req.query;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));
            if (!chat.members.find(member => member.user.toString() === user._id.toString())) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));

            const chatMember = chat.members.find(member => member.user.toString() === user._id.toString());
            const startMessage = chat.messages.find(message => message._id.toString() === messageId);

            const messages = chat.messages
                .filter(message => message.date >= chatMember.joinDate)
                .splice(chat.messages.indexOf(startMessage) || 0, limit);

            res.json(responseHandler('data', messages))
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async getChatMedia(req, res) {
        try {
            const {chatId} = req.params;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));

            if (!chat.members.find(member => member.user.toString() === user._id.toString())) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));

            let media = [];

            chat.messages.forEach(message => {
                if (!message.location || !message.location.length) {
                    media = [...media, ...message.media];
                }
            });

            res.json(responseHandler('data', media));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async hideChat(req, res) {
        try {
            const {chatId} = req.params;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));
            if (chat.isGroup) return res.json(errorHandler('chats.notPrivate', {}, req.originalUrl));

            const member = chat.members.find(member => member.user.toString() === user._id.toString());

            if (!member) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));

            member.joinDate = Date.now();
            member.isChatVisible = false;

            const chatOpponent = chat.members.find(member => member.user.toString() !== user._id.toString());

            if (!chatOpponent.isChatVisible) {//Chat hidden for both users, so we don't need it anymore
                await chat.remove();

                res.json(responseHandler('chats.removed'));
            } else {
                await chat.save();

                res.json(responseHandler('chats.joinDateReset'));
            }
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async exitChat(req, res) {
        try {
            const {chatId} = req.params;
            const {user} = req;

            if (!chatId || !ObjectId.isValid(chatId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            let chat = await Chat.findById(chatId);

            if (!chat) return res.json(errorHandler('chats.notExist', {}, req.originalUrl));

            const member = chat.members.find(member => member.user.toString() === user._id.toString());

            if (!member) return res.json(errorHandler('chats.notMember', {}, req.originalUrl));

            member.exitDate = Date.now();

            await chat.save();

            res.json(responseHandler('chats.exitDateSet'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
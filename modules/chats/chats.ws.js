const WebSocket = require('ws');
const chatsWsController = require('./chats.ws.controller');

module.exports = (server, middlewares) => {
    const wss = new WebSocket.Server({
        server,
        path: '/api/chats/ws',
        verifyClient: chatsWsController.verifyClient(middlewares.sessionParser)
    });

    wss.on('connection', (ws, req) => {
        const {user} = req;

        chatsWsController.onConnection(user, ws);

        ws.on('message', data => chatsWsController.onMessage(ws, data, user));
        ws.on('close', () => chatsWsController.onClose(user));
    });

    setInterval(function ping() {
        wss.clients.forEach(ws => ws.ping('', false, true));
    }, 30000);
};
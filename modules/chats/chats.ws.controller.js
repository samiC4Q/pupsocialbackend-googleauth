const mongoose = require('mongoose');
const User = mongoose.model('User');
const Chat = mongoose.model('Chat');
const errorHandler = require('../handlers/error.handler');
const pushNotifications = require('../notifications/push-notifications.controller');
const ObjectId = mongoose.Types.ObjectId;

const connections = new Map();

module.exports = {
    verifyClient(sessionParser) {
        return function (info, done) {
            sessionParser(info.req, {}, function () {
                const {session} = info.req;

                if (session && session.passport && session.passport.user) {
                    User.findById(info.req.session.passport.user, (err, user) => {
                        if (err) done(false);

                        info.req.user = user;
                        done(true);
                    });
                }
            });
        }
    },

    onConnection(user, ws) {
        console.info('onConnection', user._id);
        connections.set(user._id.toString(), ws);
    },


    onClose(user) {
        console.info('onClose', user._id);
        connections.delete(user._id.toString());
    },

    async onMessage(ws, data, user) {
        let messageData = {};

        try {
            messageData = JSON.parse(data);

            const {chatId} = messageData;

            if (!chatId || !ObjectId.isValid(chatId)) return ws.send(JSON.stringify(errorHandler('general.requiredFields', {}, req.originalUrl)));

            let chat = await Chat.findById(chatId);

            if (!chat) return ws.send(JSON.stringify(errorHandler('chats.notExist', {}, req.originalUrl)));
            if (!chat.members.find(member => member.user.toString() === user._id.toString())) ws.send(JSON.stringify(errorHandler('chats.notMember', {}, req.originalUrl)));

            const newMessage = Object.assign({}, messageData, {
                author: user._id
            });

            switch (messageData.actionType) {
                case 'startTyping':
                case 'endTyping':
                    chat.members.map(member => member.user).forEach(memberId => {
                        const memberSocket = connections.get(memberId.toString());

                        if (memberSocket) { //User online. We can send him message
                            memberSocket.send(JSON.stringify(newMessage), error => {
                                if (error) console.error(error);
                            });
                        }
                    });
                    break;
                case 'message':
                    chat.messages.push(newMessage);

                    if (!chat.isGroup) {
                        const chatOpponent = chat.members.find(member => member.user.toString() !== user._id.toString());

                        chatOpponent.isChatVisible = true;
                    }

                    //type 2 is equal to media type
                    if (newMessage.type === 2 && newMessage.media && newMessage.media.length) {
                        chat.sharedMediaCount += newMessage.media.length;
                    }

                    await chat.save();

                    chat.members
                        .map(member => member.user)
                        .forEach(async memberId => {
                            const memberSocket = connections.get(memberId.toString());

                            let message = await User.populate(newMessage, {
                                path: 'author',
                                select: 'name image_url'
                            });
                            console.info('onMessage connections:', connections.keys());

                            if (memberSocket) {
                                console.info('Send message to member', memberId);
                                //User online. We can send him message
                                memberSocket.send(JSON.stringify(message), error => {
                                    if (error) console.error(error);
                                });
                            } else {
                                console.info('Send push to member', memberId);
                               //User offline. We should send him push notification
                                let user = await User.findById(memberId);

                                if (!chat.isGroup && user.blocked.indexOf(newMessage.author._id) !== -1) return;

                                let body = '';

                                if (newMessage.type === 2 && newMessage.media && newMessage.media.length) {
                                    body = `Photo`;
                                } else if (newMessage.type === 3) {
                                    body = `Shared ${user.gender ? 'his' : 'her'} location`;
                                } else {
                                    body = `Sent a message`;
                                }
                                console.info('Push body', body, user.pushToken);
                                pushNotifications.send(user.pushToken, {
                                    title: newMessage.author.name,
                                    body,
                                    topic: 'com.pupsocialapp',
                                    launchImage: newMessage.author.image_url,
                                    payload: {
                                        route: 'banner',
                                        _id: chatId
                                    }
                                });
                            }
                        });
                    break;
            }
        } catch (err) {
            return ws.send(JSON.stringify(errorHandler('db', err)));
        }
    }
};
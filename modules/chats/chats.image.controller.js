const S3Storage = require('../amazon/amazon.storage');
const multer = require('multer');
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');

const uploader = multer({
    storage: S3Storage({
        getFilePath: function (req, file, cb) {
            // const {chatId} = req.params;
            const imageType = file.originalname.split('.').pop();

            cb(null, `uploads/chats/${req.user._id}_${Date.now()}.${imageType}`);
        }
    })
});

module.exports = {
    uploader: uploader.single('image'),

    uploadImage(req, res) {
        if (!req.file) return res.json(errorHandler('file.uploadError', {}, req.originalUrl));

        res.json(responseHandler('data', {imageUrl: req.file.Location}));
    }
};
const express = require('express');
const router = express.Router();
const chatsController = require('./chats.crud.controller');
const chatsImageController = require('./chats.image.controller');

router.get('/', chatsController.list);
router.post('/', chatsController.create);
router.get('/has-unread-messages', chatsController.hasUnreadMessages);
router.post('/upload-image', chatsImageController.uploader, chatsImageController.uploadImage);
router.get('/:chatId', chatsController.get);
router.post('/:chatId', chatsController.update);
router.delete('/:chatId', chatsController.remove);

router.get('/:chatId/messages', chatsController.getChatMessages);
router.get('/:chatId/media', chatsController.getChatMedia);
router.post('/:chatId/invite', chatsController.invite);
router.post('/:chatId/update-notification-settings', chatsController.updateNotificationSettings);
router.post('/:chatId/remove-member', chatsController.removeMember);
router.delete('/:chatId/leave', chatsController.leave);
router.post('/:chatId/hide', chatsController.hideChat);
router.post('/:chatId/exit', chatsController.exitChat);

module.exports = router;
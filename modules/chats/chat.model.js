const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatSchema = new Schema({
    name: String,
    imageUrl: String,
    isGroup: {
        type: Boolean,
        default: false
    },
    messages: [{
        type: {
            type: Number,
            enum: [0, 1, 2, 3, 4] // 0 - none, 1 - message, 2 - shared media, 3 - location, 4 - system
        },
        author: {type: Schema.Types.ObjectId, ref: 'User'},
        message: String,
        media: [String],
        date: {
            type: Date,
            default: Date.now()
        },
        generatedID: String,
        location: [Number],
    }],
    sharedMediaCount: {
        type: Number,
        default: 0
    },
    members: [{
        user: {type: Schema.Types.ObjectId, ref: 'User'},
        invitedBy: {type: Schema.Types.ObjectId, ref: 'User'},
        isPushEnabled: {
            type: Boolean,
            default: false
        },
        isCreator: {
            type: Boolean,
            default: false
        },
        joinDate: {
            type: Date,
            default: Date.now()
        },
        exitDate: {
            type: Date,
            default: Date.now()
        },
        isChatVisible: {
            type: Boolean,
            default: true
        }
    }],
    disabled: {
        type: Boolean,
        default: false
    },
    hasUnreadMessages: {
        type: Boolean,
        default: false
    }
});

ChatSchema.post('remove', function (doc) {
    //TODO: Clean images after chat was removed.
});

mongoose.model('Chat', ChatSchema);
module.exports = ChatSchema;
const amazonController = require('./amazon.controller');

function getFilePath(req, file, cb) {
    cb(null, `uploads/${file.originalname}`);
}

function S3Storage(opts) {
    this.getFilePath = (opts.getFilePath || getFilePath);
}

S3Storage.prototype._handleFile = function (req, file, cb) {
    this.getFilePath(req, file, function (err, Key) {
        if (err) return cb(err);

        amazonController.upload(file.stream, Key).then(data => cb(null, data), cb);
    });
};

S3Storage.prototype._removeFile = function (req, file, cb) {
    amazonController.remove(file.key).then(data => cb(null, data), cb);
};

module.exports = function (opts) {
    return new S3Storage(opts)
};
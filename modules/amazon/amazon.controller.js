const AWS = require('aws-sdk');
const s3 = new AWS.S3({signatureVersion: 'v4'});
const {Bucket, basePath, ACL} = require('../../config/main').amazonS3;

module.exports = {
    upload(Body, Key) {
        return s3.upload({Bucket, Key, Body, ACL}).promise();
    },

    remove(Key) {
        Key = Key.replace('https://', '').replace('http://', '');
        Key = Key.split('/');
        Key.shift();
        Key = Key.join('/');

        return s3.deleteObject({Bucket, Key}).promise();
    },

    deleteObjects(Objects) {
        const params = {
            Bucket,
            Delete: {
                Objects,
                Quiet: false
            }
        };

        return s3.deleteObjects(params).promise();
    },

    getFolderFiles(Prefix, ContinuationToken) {
        return s3.listObjects({Bucket, Prefix}).promise();
    }
};
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DogSchema = new Schema({
    name: String,
    birthday: Date,
    age: Number,
    about: String,
    breed: [String],
    images: [String],
    gender: {
        type: Number,
        enum: [0, 1] //0 - male, 1 - female
    },
    size: String
});

DogSchema.pre('save', function (next) {
    if (this.birthday) {
        const ageDifMs = Date.now() - new Date(this.birthday).getTime();
        const ageDate = new Date(ageDifMs);

        this.age = Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    next();
});

DogSchema.post('remove', function(doc) {
    //TODO: Clean images after dog was removed.
});

mongoose.model('Dog', DogSchema);
module.exports = DogSchema;
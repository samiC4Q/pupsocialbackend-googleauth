const multer = require('multer');
const S3Storage = require('../amazon/amazon.storage');
const amazonS3 = require("../amazon/amazon.controller");
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');

const uploader = multer({
    storage: S3Storage({
        getFilePath: (req, file, cb) => {
            const imageType = file.originalname.split('.').pop();

            cb(null, `uploads/dogs/${req.user._id}_${req.user.dogs.length}_${Date.now()}.${imageType}`);
        }
    })
});

module.exports = {
    uploader: uploader.single('image'),

    uploadImage(req, res) {
        if (!req.file) return res.json(errorHandler('file.uploadError'));

        res.json(responseHandler('data', {imageUrl: req.file.Location}));
    },

    removeImage(req, res) {
        if (!req.body.imageUrl) return res.json(errorHandler("general.requiredFields"));

        amazonS3.remove(req.body.imageUrl).then(
            data => res.json(responseHandler('data', data)),
            error => res.json(errorHandler('amazon', error))
        )
    }
};
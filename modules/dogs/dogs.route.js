const express = require('express');
const router = express.Router();
const dogsController = require('./dogs.controller');
const dogsImageController = require('./dogs.image.controller');

router.post('/', dogsController.addDog);
router.post('/upload-image', dogsImageController.uploader, dogsImageController.uploadImage);
router.post('/remove-image', dogsImageController.removeImage);
router.post('/:dogId', dogsController.updateDog);
router.delete('/:dogId', dogsController.removeDog);

module.exports = router;
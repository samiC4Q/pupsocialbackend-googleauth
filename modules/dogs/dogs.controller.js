const mongoose = require('mongoose');
const Dog = mongoose.model('Dog');
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');
const amazonS3 = require("../amazon/amazon.controller");
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
    async addDog(req, res) {
        try {
            let {user} = req;
            let dog = await new Dog(req.body).save();

            user.dogs.push(dog);
            await user.save();

            user = await user.populate('dogs').execPopulate();

            res.json(responseHandler('data', user.dogs));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async updateDog(req, res) {
        try {
            const {dogId} = req.params;

            if (!dogId || !ObjectId.isValid(dogId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (req.user.dogs.indexOf(dogId) === -1) return res.json(errorHandler('user.notDogOwner', {}, req.originalUrl));

            let dog = await Dog.findById(dogId);

            if (!dog) return res.json(errorHandler('dog.notFound', {}, req.originalUrl));

            dog.set(req.body);
            await dog.save();

            res.json(responseHandler('data', dog));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async removeDog(req, res) {
        try {
            let {user} = req;
            const {dogId} = req.params;

            if (!dogId || !ObjectId.isValid(dogId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (user.dogs.indexOf(dogId) === -1) return res.json(errorHandler('user.notDogOwner', {}, req.originalUrl));

            let dog = await Dog.findById(dogId);

            if (!dog) return res.json(errorHandler('dog.notFound', {}, req.originalUrl));

            await dog.images.map(async imageUrl => await amazonS3.remove(imageUrl));

            await dog.remove();

            user.dogs.pull(dogId);
            user = await user.save();

            user = await user.populate('dogs').execPopulate();

            res.json(responseHandler('data', user.dogs));
        } catch (err) {
            return res.json(errorHandler('db', err));
        }
    }
};
const express = require('express');
const router = express.Router();

const complaintsController = require('./complaints.controller');

router.post('/user', complaintsController.user);
router.post('/image', complaintsController.image);

module.exports = router;
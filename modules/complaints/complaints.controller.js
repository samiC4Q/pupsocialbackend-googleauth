const mongoose = require('mongoose');
const Complaint = mongoose.model('Complaint');
const ObjectId = mongoose.Types.ObjectId;
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');

module.exports = {
    async user(req, res) {
        try {
            const {user} = req;
            const {userId} = req.body;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            const complaintQuery = {
                reporter: user._id,
                user: userId
            };

            let complaint = await Complaint.findOne(complaintQuery);

            if (!complaint) {
                await new Complaint(complaintQuery).save();
            }

            res.json(responseHandler('complaints.user'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async image(req, res) {
        try {
            const {user} = req;
            const {imageUrl} = req.body;

            if (!imageUrl) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            const complaintQuery = {
                reporter: user._id,
                imageUrl
            };

            let complaint = await Complaint.findOne(complaintQuery);

            if (!complaint) {
                await new Complaint(complaintQuery).save();
            }

            res.json(responseHandler('complaints.image'));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    }
};
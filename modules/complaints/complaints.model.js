const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const complaintsSchema = new Schema({
    reporter: {type: Schema.Types.ObjectId, ref: 'Dog'},
    user: {type: Schema.Types.ObjectId, ref: 'Dog'},
    imageUrl: String
});

mongoose.model('Complaint', complaintsSchema);
module.exports = complaintsSchema;
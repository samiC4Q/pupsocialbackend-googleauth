const responseHandler = require('../handlers/response.handler');

module.exports = {
    login(req, res) {
        res.setHeader('session_id', req.sessionID);
        res.json(responseHandler('data', req.sessionID));
    },

    logout(req, res) {
        req.session.destroy();
        res.json(responseHandler('data', 'Log out complete'));
    }
};
const express = require('express');
const router = express.Router();
const authController = require('./auth.controller');
const facebookToken = require('./../facebook/facebook-token');
const google = require('./../google/google-token');


// facebook login
router.post('/login', facebookToken.authenticate, authController.login);
router.post('/logout', authController.logout);

// google login
router.post('/login', google.authenticate, authController.login);
router.post('/logout', authController.logout);




module.exports = router;
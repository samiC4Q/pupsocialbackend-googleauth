const mongoose = require('mongoose');
const User = mongoose.model('User');
const Dog = mongoose.model('Dog');
const Chat = mongoose.model('Chat');
const S3 = require('../amazon/amazon.controller');
const errorHandler = require('../handlers/error.handler');

module.exports = {
    async deleteAllUnusedFiles() {
        try {
            await this.deleteUnusedAvatars();
            await this.deleteUnusedDogsImages();
            await this.deleteUnusedChatsImages();
        } catch (err) {
            errorHandler('custom', err.message);
        }
    },

    async deleteUnusedAvatars() {
        try {
            let users = await User.find({}, 'image_url');
            const usedAvatars = users.map(user => user.image_url);

            this.deleteUnusedFiles('avatars', usedAvatars);
        } catch (err) {
            errorHandler('custom', err.message);
        }
    },

    async deleteUnusedDogsImages() {
        try {
            let dogs = await Dog.find({}, 'images');
            const dogsImages = dogs.map(dog => dog.images.join(' '));

            this.deleteUnusedFiles('uploads/dogs', dogsImages);
        } catch (err) {
            errorHandler('custom', err.message);
        }
    },

    async deleteUnusedChatsImages() {
        try {
            let chats = await Chat.find({}, 'imageUrl messages.media');
            const chatsImages = [];

            chats.forEach(chat => {
                if (chat.imageUrl) {
                    chatsImages.push(chat.imageUrl);
                }

                chat.messages.forEach(message => {
                    if (message.media && message.media.length) {
                        chatsImages.push(...message.media);
                    }
                });
            });

            this.deleteUnusedFiles('uploads/chats', chatsImages);
        } catch (err) {
            errorHandler('custom', err.message);
        }
    },

    async deleteUnusedFiles(folder, filesInUse) {
        try {
            const filesInUseString = filesInUse.join(' ');

            let files = await S3.getFolderFiles(folder);

            const unusedFiles = files.Contents
                .filter(file => {
                    return !new RegExp(file.Key).test(filesInUseString);
                })
                .map(file => {
                    return {
                        Key: file.Key
                    }
                });

            if (unusedFiles.length) {
                let data = await S3.deleteObjects(unusedFiles);

                console.log(`GC: ${data.Deleted.length} files was cleared`);
            }
        } catch (err) {
            errorHandler('custom', err.message);
        }
    }
};
const CronJob = require('cron').CronJob;
const mongoose = require('mongoose');
const User = mongoose.model('User');

new CronJob('00 00 00 * * 0-6', () => {
    User.update(
        {},
        {$set: {"hangouts.freeAvailable": 25}},
        {"multi": true},
        (err) => {
            if (err) console.error(err);
        }
    );
}, null, true, 'America/New_York');
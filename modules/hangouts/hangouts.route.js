const express = require('express');
const router = express.Router();

const hangoutsController = require('./hangouts.controller');

router.get('/', hangoutsController.list);
router.post('/send-hangout/:userId', hangoutsController.sendHangout);
router.post('/accept-hangout/:userId', hangoutsController.acceptHangout);
router.delete('/decline-hangout/:userId', hangoutsController.declineHangout);
router.post('/validate-purchase', hangoutsController.validatePurchase);

module.exports = router;
const CronJob = require('cron').CronJob;
const garbageCollector = require('../garbage-collector/garbage-collector.controller');

new CronJob('00 00 00 * * 0-6', () => {
    garbageCollector.deleteAllUnusedFiles();
}, null, true, 'America/New_York');
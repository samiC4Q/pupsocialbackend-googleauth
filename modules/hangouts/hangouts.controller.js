const mongoose = require('mongoose');
const User = mongoose.model('User');
const Chat = mongoose.model('Chat');
const errorHandler = require('../handlers/error.handler');
const responseHandler = require('../handlers/response.handler');
const iap = require('in-app-purchase');
const config = require('../../config/main');
const pushNotifications = require('../notifications/push-notifications.controller');
const ObjectId = mongoose.Types.ObjectId;

iap.config({
    test: true,
    verbose: false
});

module.exports = {
    list(req, res) {
        res.json(responseHandler('data', user.hangouts));
    },

    async sendHangout(req, res) {
        try {
            const {userId} = req.params;
            const {user} = req;

            if (!userId || !ObjectId.isValid(userId)) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));
            if (!user.hangouts.freeAvailable && !user.hangouts.purchasedAvailable) return res.json(errorHandler('hangouts.notEnough', {}, req.originalUrl));

            let opponent = await User.findById(userId);

            if (!opponent) return res.json(errorHandler('user.notFound', {}, req.originalUrl));

            const hangoutInvite = opponent.hangoutInvites.find(userId => userId.toString() === user._id.toString());

            if (hangoutInvite) return res.json(errorHandler('hangouts.alreadySent', {}, req.originalUrl));

            user.hangouts.freeAvailable ? user.hangouts.freeAvailable -= 1 : user.hangouts.purchasedAvailable -= 1;

            await user.save();

            opponent.hangoutInvites.push(user._id);
            opponent.notifications.unshift({
                type: 1,
                from: user._id
            });

            await opponent.save();

            opponent = await opponent.populateFields();

            pushNotifications.send(opponent.pushToken, {
                title: user.name,
                body: 'Sent a hangout request',
                payload: {
                    imageUrl: user.image_url,
                    route: 'popup',
                    type: 1,
                    userId: user._id
                }
            });

            res.json(responseHandler('data', opponent));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async declineHangout(req, res) {
        try {
            const {userId} = req.params;
            let {user} = req;

            if (!userId) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            const notification = user.notifications.find(notification => notification.type === 1 && notification.from.toString() === userId);

            user.hangoutInvites.pull(userId);
            user.notifications.pull(notification);

            await user.save();

            user = await user.populateFields();

            res.json(responseHandler('data', user));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async acceptHangout(req, res) {
        try {
            const {userId} = req.params;
            let {user} = req;

            if (!userId) return res.json(errorHandler('general.requiredFields', {}, req.originalUrl));

            const notification = user.notifications.find(notification => (notification.from.toString() === userId.toString() && notification.type == 1));

            if (!notification) return res.json(errorHandler('hangouts.notInvited', {}, req.originalUrl));

            const data = {
                members: [{
                    user: user,
                    invitedBy: user,
                    isCreator: true
                }, {
                    user: notification.from,
                    invitedBy: user,
                    isCreator: false
                }],
                messages: [
                    {
                        type: 4,
                        message: 'Let\'s hang out!',
                        date: Date.now()
                    }
                ],
                isGroup: false
            };

            let chat = await Chat.create(data);
            let opponent = await User.findById(notification.from);

            opponent.notifications.unshift({
                type: 2,
                from: user._id,
                payload: {
                    chatId: chat._id
                }
            });

            await opponent.save();

            notification.type = 8;
            notification.updatedAt = Date.now();
            notification.payload = {
                chatId: chat._id
            };

            user.hangoutInvites.pull(userId);

            await user.save();

            user = await user.populateFields();

            let notifications = await user.populateNotifications();

            pushNotifications.send(opponent.pushToken, {
                title: user.name,
                body: 'Accepted your hangout request',
                payload: {
                    imageUrl: user.image_url,
                    route: 'popup',
                    type: 2,
                    userId: user._id,
                    chatId: chat._id
                }
            });

            res.json(responseHandler('data', notifications));
        } catch (err) {
            return res.json(errorHandler('db', err, req.originalUrl));
        }
    },

    async validatePurchase(req, res) {
        const {receipt} = req.body;
        let {user} = req;

        await iap.validate(receipt, async (err, validatedData) => {
            if (err) return res.json(errorHandler('db', err, req.originalUrl));

            try {
                const {receipt = {}} = validatedData;
                const {in_app = []} = receipt;
                const [product = {}] = in_app;
                const {product_id = ''} = product;

                user.hangouts.purchasedAvailable += config.hangoutsPurchase[product_id] || 0;

                await user.save();

                user = user.populateFields();

                res.json(responseHandler('data', user));
            } catch (err) {
                return res.json(errorHandler('db', err, req.originalUrl));
            }
        });
    }
};
const express = require('express');
const router = express.Router();
const config = require('../../config/main');

const adminController = require('./admin.controller');

const isAuthenticated = (req, res, next) => {
    const auth = req.get("authorization");

    if (!auth) {
        res.set("WWW-Authenticate", "Basic realm=\"Authorization Required\"");

        return res.status(401).send("Authorization Required");
    } else {
        const credentials = new Buffer(auth.split(" ").pop(), "base64").toString("ascii").split(":");

        if (credentials[0] === config.adminLogin && credentials[1] === config.adminPassword) {
            next();
        } else {
            return res.status(401).send("Access Denied (incorrect credentials)");
        }
    }
};

router.get('/config', isAuthenticated, adminController.getConfig);
router.post('/config', isAuthenticated, adminController.saveConfig);

module.exports = router;
const jsonfile = require('jsonfile');
const path = require('path');
const mongoose = require('mongoose');
const Config = mongoose.model('Config');
const errorHandler = require('../handlers/error.handler');

module.exports = {
    getConfig(req, res) {
        Config.findOne({}, (error, config) => {
            if (error) return res.json(errorHandler('db', error, req.originalUrl));

            res.render('config', {
                config: Object.assign({}, config.toObject(), {
                    terms: config['terms&conditionsURL']
                })
            });
        });
    },

    saveConfig(req, res) {
        const {body} = req;
        const {newDogBreed} = body;

        Config.findOne({}, (error, config) => {
            if (error) return res.json(errorHandler('db', error, req.originalUrl));

            config.set(body);

            if (newDogBreed) {
                config.dog_breeds.push(newDogBreed);
            }

            config.save(error => {
                if (error) return res.json(errorHandler('db', error, req.originalUrl));

                res.redirect('/admin/config');
            });
        });
    }
};
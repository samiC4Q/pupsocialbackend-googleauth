const express = require('express');
const passport = require("passport");

function isAuthenticated(req, res, next) {
    req.isAuthenticated() ? next() : res.json({status: false, data: "Unauthorized"});
}

module.exports = function (app) {
    app.use('/api/auth', require('./modules/auth/auth.route'));
    app.use('/api/users', isAuthenticated, require('./modules/users/users.route'));
    app.use('/api/dogs', isAuthenticated, require('./modules/dogs/dogs.route'));
    app.use('/api/search', isAuthenticated, require('./modules/search/search.route'));
    app.use('/api/chats', isAuthenticated, require('./modules/chats/chats.route'));
    app.use('/api/hangouts', isAuthenticated, require('./modules/hangouts/hangouts.route'));
    app.use('/api/complaints', isAuthenticated, require('./modules/complaints/complaints.route'));

    app.use('/develop', require('./modules/develop/develop.route'));
    app.use('/admin', require('./modules/admin/admin.route'));
    app.use('/config', require('./modules/config/config.route'));
    app.use('/google', require('./modules/users/users.route'));

    app.use('/api', isAuthenticated, express.static('public'));

    app.use('/google', passport.authenticate('google', {scope: ['profile', 'email']}));
    app.use('auth/google/callback', 
    passport.authenticate('google', { failureRedirect: '/' }),(req, res) => {
    res.redirect('/');
  });

};
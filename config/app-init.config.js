const dogBreeds = require('./dog-breeds');

module.exports = {
    "name": "app-config",
    "maxVersion": "1.1",
    "minVersion": "1",
    "terms&conditionsURL": "http://www.pupsocialapp.com/terms",
    "privacyURL": "http://www.pupsocialapp.com/privacy",
    "supportEmail": "support.pupsocial@gmail.com",
    "privacyUrl": "http://www.pupsocialapp.com/privacy",
    "termsUrl": "http://www.pupsocialapp.com/terms",
    "dog_breeds": dogBreeds,
    "dog_sizes": [
        "Small",
        "Medium",
        "Large"
    ]
};